UMGC/UMII Ensembl Reference Genomes
-----------------------------------

The Genomics Center (UMGC) and Informatics Institute (UMII) maintain local copies of Ensembl reference genomes for more than 30 species, publicly accessible on the MSI filesystem at /home/umii/public/ensembl.

There is a folder for each species, named Genus_species::

 /home/umii/public/ensembl/
   Arabidopsis_thaliana/
   Bos_taurus/
   Caenorhabditis_elegans/
   ...

Each species folder contains folders for one or more genome assemblies, with a “current” symlink pointing to the assembly currently used as the primary assembly on the Ensembl website::

 /home/umii/public/ensembl/Sus_scrofa/
   current -> Sscrofa11.1-ensembl/
   Sscrofa10.2-ensembl/
   Sscrofa11.1-ensembl/

Each assembly folder contains a reference fasta file, annotation data, and indexes for the assembly. Ensembl releases updated annotation for each assembly quarterly. The “Annotation” folder contains folders for recent Ensembl releases, with a “current” symlink pointing to the most recent release::

 /home/umii/public/ensembl/Homo_sapiens/GRCh38-ensembl/
   seq/
     genome.fa
     genome.fa.fai
     genome.fa.dict
   Annotation/
     current -> 90/
     90/
       cdna.fa
       annotation.gtf
       annotation.gff3
   bowtie2/
     genome (index basename)
   bwa/
     genome (index basename)
   hisat2/
     genome (index basename)

The consistent directory structure make it easy to locate reference files and switch between species. Most recent human and mouse genome fasta files::

 /home/umii/public/ensembl/Homo_sapiens/current/seq/genome.fa
 /home/umii/public/ensembl/Mus_musculus/current/seq/genome.fa

Most recent human GTF annotation file::

 /home/umii/public/ensembl/Homo_sapiens/current/Annotation/current/annotation.gtf

GTF annotation file from ensembl release 87 for the older pig assembly Sscrofa10.2::

 /home/umii/public/ensembl/Sus_scrofa/Sscrofa10.2-ensembl/Annotation/87/annotation.gtf


Ensembl Module
--------------

UMGC/UMII Ensembl reference genomes can be directly accessed on MSI systems using full file paths, or you can load a module that defines a set of environment variables that point to the reference data.

Load the “umgc” module to make the ensembl module available, then load the ensembl module::

 module load umgc
 module load ensembl

Load the current human genome. Either Genus_species or the common species name works, these are equivalent::

 module load Homo_sapiens
 module load human

These environment variables are now defined::

 $COMMON_NAME =     Human
 $LATIN_NAME = 	    Homo_sapiens
 $BUILD_NAME = 	    GRCh38-ensembl
 $ENSEMBL_RELEASE = 90
 $GENOME_FA =       /home/umii/public/ensembl/Homo_sapiens/GRCh38-ensembl/seq/genome.fa
 $GENOME_FAI = 	    /home/umii/public/ensembl/Homo_sapiens/GRCh38-ensembl/seq/genome.fa.fai
 $GENOME_DICT =	    /home/umii/public/ensembl/Homo_sapiens/GRCh38-ensembl/seq/genome.dict
 $CDNA_FA =   	    /home/umii/public/ensembl/Homo_sapiens/GRCh38-ensembl/Annotation/90/annotation.cdna
 $ANNOTATION_GTF =  /home/umii/public/ensembl/Homo_sapiens/GRCh38-ensembl/Annotation/90/annotation.gtf
 $ANNOTATION_GFF3 = /home/umii/public/ensembl/Homo_sapiens/GRCh38-ensembl/Annotation/90/annotation.gff3
 $BOWTIE2_INDEX =   /home/umii/public/ensembl/Homo_sapiens/GRCh38-ensembl/bowtie2/genome
 $BWA_INDEX =  	    /home/umii/public/ensembl/Homo_sapiens/GRCh38-ensembl/bwa/genome
 $HISAT2_INDEX =    /home/umii/public/ensembl/Homo_sapiens/GRCh38-ensembl/hisat2/genome

The variables can be used in place of full paths on the command-line or in bash scripts::

 bwa mem $BWA_INDEX sample1_R1.fastq > sample1.sam

Or in python scripts::

 import os
 import subprocess
 print(f"Aligning to {os.environ['COMMON_NAME']} genome build {os.environ['BUILD_NAME']} with bwa")
 subprocess.call(f"bwa mem {os.environ['BWA_INDEX']} sample1_R1.fastq > sample1.sam", shell=True)

Loading a species module loads the current reference and annotation data. Specific builds and annotation sets can be loaded if you want to load older data, or if you want to ensure the reference data you are using doesn't change when Ensembl releases new data::

 module load pig/Sscrofa10.2-ensembl

Run *ensembl* or *module avail* to see a full list of available genome builds and modules, and run *module show human* to see the environment variables set by the module. Loading a new species module will replace any previously loaded species module.

Support
.......

Reference genomes are provided by the UMGC and UMII. Visit http://bitbucket.org/jgarbe/gopher-genomes for the most up-to-date documentation, and use the associated issue tracker to report problems, provide suggestions, and request the download of additional genomes from Ensembl. Genomes from other sources are not supported.